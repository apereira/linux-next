# SPDX-License-Identifier: GPL-2.0-only
#
# Chelsio inline crypto configuration
#

config CHELSIO_INLINE_CRYPTO
	bool "Chelsio Inline Crypto support"
	default y
	help
	  Enable support for inline crypto.
	  Allows enable/disable from list of inline crypto drivers.

if CHELSIO_INLINE_CRYPTO

config CRYPTO_DEV_CHELSIO_TLS
	tristate "Chelsio Crypto Inline TLS Driver"
	depends on CHELSIO_T4
	depends on TLS_TOE
	help
	  Support Chelsio Inline TLS with Chelsio crypto accelerator.
	  Enable inline TLS support for Tx and Rx.

	  To compile this driver as a module, choose M here: the module
	  will be called chtls.

config CHELSIO_IPSEC_INLINE
       tristate "Chelsio IPSec XFRM Tx crypto offload"
       depends on CHELSIO_T4
       depends on XFRM_OFFLOAD
       depends on INET_ESP_OFFLOAD || INET6_ESP_OFFLOAD
       help
        Support Chelsio Inline IPsec with Chelsio crypto accelerator.
        Enable inline IPsec support for Tx.

        To compile this driver as a module, choose M here: the module
        will be called ch_ipsec.

endif # CHELSIO_INLINE_CRYPTO
